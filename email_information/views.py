import imaplib
import json

from services import table, subtitle, information

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django.conf import settings
from django.http import JsonResponse

from email import message_from_bytes

class ReadEmail(APIView):
    def get(self, request):
        data = []

        # Connect server
        try:
            imap = imaplib.IMAP4_SSL(settings.IMAP_SERVER, 993)
            imap.login(settings.EMAIL, settings.PASSWORD)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
        
        # Retreive Email
        mailbox = "INBOX"
        imap.select(mailbox)
        status, messages = imap.search(None, "UNSEEN", "TEXT ANNOUNCEMENT")
        message_ids = messages[0].split()
        for message_id in message_ids:
            status, msg_data = imap.fetch(message_id, "(RFC822)")

            message = message_from_bytes(msg_data[0][1])
            
            email_body = None
            if message.is_multipart():
                for part in message.walk():
                    if part.get_content_type() == 'text/html':
                        email_body = part.get_payload(decode=True).decode()
                        break
            else:
                email_body = message.get_payload(decode=True).decode()

            # print(email_body)            
            subtitle_data = subtitle.process_subtitle(message.get("Subject"))

            table_data = None
            if subtitle_data["email_type"] == "DELAY_DUE_TO" or subtitle_data["email_type"] == "AMENDED":
                table_data = table.process_issuer_code(email_body)
            elif subtitle_data["email_type"] == "UPDATED":
                table_data = table.process_update_table(email_body)
            elif subtitle_data["email_type"] == "NORMALIZED":
                table_data = table.process_normalized(email_body)
            elif subtitle_data["email_type"] == "DELAY":
                table_data = table.process_delay(email_body)

            information_data = information.process_information(email_body)
            data.append({
                "From": message.get("From") if information_data["sender"] is None else information_data["sender"],
                "To": message.get("To"),
                "Date": message.get("Date"),
                "Subject": subtitle_data["title"],
                "Email_Type": subtitle_data["email_type"],
                "To": message.get("To"),
                "Table_data": table_data,
                "Start": information_data["start"],
                "End": information_data["end"],
                # "Range_Time": information_data["range_time"]
            })
            
            imap.store(message_id, "-FLAGS", "\\Seen")
        # Close the connection
        imap.close()
        imap.logout()

        return Response(data)
