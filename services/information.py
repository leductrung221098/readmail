from bs4 import BeautifulSoup

def process_information(email_body):
    data = {"sender": None, "start": None, "end": None, "range_time": None}
    soup = BeautifulSoup(email_body, 'html.parser')

    paragraphs = soup.find_all('strong', class_="gmail_sendername")
    anchor_tags = soup.find_all('a', href=lambda href: href and '.com' in href)
    if len(paragraphs) == 1:
        data["sender"] = paragraphs[0].get_text()
        # data["sender"] += ' ' + anchor_tags[0].get_text()
    elif len(paragraphs) == 2:
        data["sender"] = paragraphs[1].get_text()
        # data["sender"] += ' ' + anchor_tags[1].get_text()

    paragraphs = soup.find_all('div', attrs={'dir': 'ltr'})
    for p in paragraphs:
        text = p.get_text()
        start_index = text.find("Start")
        end_index = text.find("Affected")
        if start_index == -1 or end_index == -1:
            continue
        # data["range_time"] = text[start_index:end_index].replace("End", " End")
        range_time_text = text[start_index:end_index]
        end_at_index = range_time_text.find("End")
        for i in range(0, end_at_index):
            if range_time_text[i].isnumeric():
                data["start"] = range_time_text[i:end_at_index]
                break
        for i in range(end_at_index, len(range_time_text)):
            if range_time_text[i].isnumeric():
                data["end"] = range_time_text[i:]
                break
        break
    return data