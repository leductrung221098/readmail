def process_subtitle(title):
    data = {}
    # title = "Fwd: [ANNOUNCEMENT] Delay In Processing Due to .... "
    # title = "[ANNOUNCEMENT] Delay In Processing Due to .... "
    if title[0:3] == 'Fwd':
        title = title[20:]
    else:
        title = title[15:]
    data["title"] = title
    if "UPDATED" in title:
        data["email_type"] = "UPDATED"
    elif "NORMALIZED" in title:
        data["email_type"] = "NORMALIZED"
    elif "AMENDED" in title:
        data["email_type"] = "AMENDED"
    elif "Delay In Processing - " in title:
        data["email_type"] = "DELAY"
    else:
        data["email_type"] = "DELAY_DUE_TO"
        
    return data