from bs4 import BeautifulSoup

def process_issuer_code(email_body):
    issuer_codes_data = []
    affected_issuer_codes = []
    if email_body:
        soup = BeautifulSoup(email_body, 'html.parser')
        paragraphs = soup.find_all('td')
        for p in paragraphs:
            issuer_codes_data.append(p.get_text())

    issuer_codes_data = issuer_codes_data[2:-1]
    for i in range(0, len(issuer_codes_data), 2):
        if len(issuer_codes_data[i]) < 10:
            affected_issuer_codes.append({
                'issuer_code': issuer_codes_data[i],
                'name': issuer_codes_data[i+1]
            })
    return affected_issuer_codes

def process_update_table(email_body):
    data = []
    response = []
    if email_body:
        soup = BeautifulSoup(email_body, 'html.parser')
        paragraphs = soup.find_all('td')
        for p in paragraphs:
            data.append(p.get_text())
    data = data[7:-1]
    for i in range(0, len(data), 7):
        response.append({
            'start_time': data[i] + ' ' + data[i+1],
            'end_time': data[i+2] + ' ' + data[i+3],
            'duration': data[i+4],
            'issuer_code': data[i+5],
            'channel': data[i+6]
        })
    return response

def process_normalized(email_body):
    data = []
    response = []
    if email_body:
        soup = BeautifulSoup(email_body, 'html.parser')
        paragraphs = soup.find_all('td')
        for p in paragraphs:
            data.append(p.get_text())
    data = data[3:-1]
    for i in range(0, len(data), 3):
        response.append({
            'normalized_channel': data[i],
            'do_transfer_type_value': data[i+1],
            'issuer_code': data[i+2]
        })
    return response

def process_delay(email_body):
    data = []
    response = []
    if email_body:
        soup = BeautifulSoup(email_body, 'html.parser')
        paragraphs = soup.find_all('td')
        for p in paragraphs:
            data.append(p.get_text())
    data = data[3:-1]
    for i in range(0, len(data), 3):
        response.append({
            'affected_channel': data[i],
            'do_transfer_type_value': data[i+1],
            'issuer_code': data[i+2]
        })
    return response